const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighter(fighter) {
        if (fighter) {
          return fighter;
        } else {
          return null;
        }
    };
}

module.exports = new FighterService();