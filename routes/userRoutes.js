const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.post('/', function(req, res, next) {
    createUserValid(req.body);
    
    const result = UserService.getUser(req.body);
    if (result) {
    res.status(200).send(JSON.stringify(result));
    } else {
    res.status(400).send(`Some error`);
    }
});
module.exports = router;