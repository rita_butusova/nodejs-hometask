const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const { id, name, health, power, defense } = req;
    if (name && power && defense) {
        user.id = id;
        user.name = name;
        user.health = health;
        user.power = power;
        user.defense = defense;

    }
    /*
    app.get('/api/fighters', function (req, res) {
        res.send(fighters);
    })
    
    app.get('/api/fighters/:id', function (req, res) {
        var fighter = fighters.find(function(user) {
            return fighter.id === Number(req.params.id)
        });
        res.send(fighter)
    })
    */

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    /*app.post('/api/fighters', function (req, res) {
        var fighter = {
            id: req.body.id,
            name: req.body.name,
            health: req.body.health,
            power: req.body.power,
            defense: req.body.defense,
        };
        res.send(fighter);
    });

    app.put('/api/fighters/:id', function (req, res) {
        var fighter = fighters.find(function(fighter) {
            return fighter.id === Number(req.params.id)
        });
        fighter.name = req.body.name;
        fighter.health = req.body.health,
        fighter.power = req.body.power,
        fighter.defense = req.body.defense,
        res.send(fighter);
    })
    app.delete('/api/fighters/:id', function (req, res) {
        fighters = fighters.filter(function (fighter) {
            return fighter.id !== Number(req.params.id);
        })
        res.sendStatus(200);
    })*/
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;