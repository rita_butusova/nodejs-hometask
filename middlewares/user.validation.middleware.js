const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const { id, firstName, lastName, email, phoneNumber, password } = req;
    if (firstName && lastName && email && phoneNumber && password) {
        user.id = id;
        user.firstName = firstName;
        user.lastName = lastName;
        user.email = email;
        user.phoneNumber = phoneNumber;
        user.password = password

    }
    next();
/*    app.get('/api/users', function (req, res) {
        res.send(user);
    })
    
    app.get('/api/users:id', function (req, res) {
        var user = users.find(function(user) {
            return user.id === Number(req.params.id)
        });
        res.send(user)
    })
  */  

}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    /*app.post('/api/users', function (req, res) {
        var user = {
            id: req.body.id,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            phoneNumber: req.body.phoneNumber,
            password: req.body.password
        };
        res.send(user);
    });

    app.put('/api/users/:id', function (req, res) {
        var user = users.find(function(user) {
            return user.id === Number(req.params.id)
        });
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName,
        user.email = req.body.email,
        user.phoneNumber = req.body.phoneNumber,
        user.password = req.body.password
        res.send(user);
    })
    app.delete('/api/users/:id', function (req, res) {
        users = users.filter(function (user) {
            return user.id !== Number(req.params.id);
        })
        res.sendStatus(200);
    })
*/
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;