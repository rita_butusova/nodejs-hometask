const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   if (
    req.firstName &&
    req.lastName &&
    req.email &&
    req.phoneNumber &&
    req.password
  ) {
    next();
  } else {
    res.status(404).send({
        error: true,
        message: 'User not found'
    })
  }
   next();
}

exports.responseMiddleware = responseMiddleware;